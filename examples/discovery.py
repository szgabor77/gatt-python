import gatt

class AnyDeviceManager(gatt.DeviceManager):
    def device_discovered(self, device):
        print("[%s] Discovered, alias = %s, RSSI = %s, Manuf = %s" % (device.mac_address, device.alias(), str(device.rssi()), str(device.manufData()) ) )

manager = AnyDeviceManager(adapter_name='hci0')
manager.start_discovery()
manager.run()
