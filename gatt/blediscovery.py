import gatt
import datetime
import threading

class BLEdiscoveryManager(gatt.DeviceManager):
    def __init__(self, adapter_name):
        gatt.DeviceManager.__init__(self, adapter_name)
        self.devlist = {}

    def device_discovered(self, device):
        #print("[%s] Discovered, alias = %s, RSSI = %s, Manuf = %s" % (device.mac_address, device.alias(), str(device.rssi()), str(device.manufData()) ) )

        if device.mac_address not in self.devlist.keys():
            print("New device %s" % (device.mac_address) )
            newdev = {}
            newdev["mac"] = device.mac_address
            newdev["timestamp"] = datetime.datetime.now()
            newdev["rssi"] = device.rssi()
            self.devlist[device.mac_address] = newdev
        else:
            #print("Known device %s" % (device.mac_address) )
            self.devlist[device.mac_address]["timestamp"] = datetime.datetime.now()
            self.devlist[device.mac_address]["rssi"] = device.rssi()

        return

    def getDevices(self):
        print("Discovered devices: %i\n" % (len(self.devlist)) )
        for mac, dev in self.devlist.items():
            print("  [%s] RSSI: %s, Timestamp: %s" % (mac, str(dev["rssi"]), str(dev["timestamp"])) )

        return

if __name__ == "__main__":
    dmanager = BLEdiscoveryManager(adapter_name='hci0')
    dmanager.start_discovery()
    dmanager.run()
